var http = require("http");
var fs = require('fs')
var qs = require("querystring")
var users = [];
var pawns=[
    [1,0,1,0,1,0,1,0],
    [0,1,0,1,0,1,0,1],
    [0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0],
    [2,0,2,0,2,0,2,0],
    [0,2,0,2,0,2,0,2]
];
var ruch='white'
var restet_game=()=>{
    pawns=[
        [1,0,1,0,1,0,1,0],
        [0,1,0,1,0,1,0,1],
        [0,0,0,0,0,0,0,0],
        [0,0,0,0,0,0,0,0],
        [0,0,0,0,0,0,0,0],
        [0,0,0,0,0,0,0,0],
        [2,0,2,0,2,0,2,0],
        [0,2,0,2,0,2,0,2]
    ];
    ruch='white'
}
var server = http.createServer(function (req, res) {
    console.log(req.method + " : " + req.url)

    switch (req.method) {
        case "GET": {
            switch (req.url) {
                case "/": {
                    fs.readFile("public/html/index.html", function (error, data) {
                        res.writeHead(200, { 'Content-Type': 'text/html' });
                        res.write(data);
                        res.end();
                    })
                    break;
                }
                case "/wait": {
                    fs.readFile("public/html/wait.html", function (error, data) {
                        res.writeHead(200, { 'Content-Type': 'text/html' });
                        res.write(data);
                        res.end();
                    })
                    break;
                }
                case "/game": {
                    fs.readFile("public/html/game.html", function (error, data) {
                        res.writeHead(200, { 'Content-Type': 'text/html' });
                        res.write(data);
                        res.end();
                    })
                    break;
                } case "/cookies.js": {
                    fs.readFile("public/javascript/cookies.js", function (error, data) {
                        res.writeHead(200, { 'Content-Type': 'text/html' });
                        res.write(data);
                        res.end();
                    })
                    break;
                }
                case "/three.js": {
                    fs.readFile("public/javascript/three.js", function (error, data) {
                        res.writeHead(200, { 'Content-Type': 'text/html' });
                        res.write(data);
                        res.end();
                    })
                    break;
                }
                default: {
                    fs.readFile("public/html/404.html", function (error, data) {
                        res.writeHead(404, { 'Content-Type': 'text/html' });
                        res.write(data);
                        res.end();
                    })
                    break;
                }
            }
            break;
        }
        case "POST": {
            switch (req.url) {
                case "/login": {
                    var sendbackdata = {};
                    var allData = "";
                    req.on("data", function (data) {
                        console.log("data: " + data)
                        allData += data;
                    })
                    req.on("end", function (data) {
                        var finish = qs.parse(allData)
                        if (users.length < 2) {
                            try {
                                if (users[0].nick == finish.nick) {
                                    sendbackdata.status = "gracz o podanym nicku już isnieje"
                                    res.end(JSON.stringify(sendbackdata));
                                    return;
                                }
                            } catch (e) {
                            }
                            sendbackdata.nick = finish.nick;
                            sendbackdata.status = "ok"
                            sendbackdata.session = generate_ssid();
                            if (users.length == 0) {
                                sendbackdata.color = "white"
                            } else {
                                sendbackdata.color = "black"
                            }
                            users.push(finish)
                            res.end(JSON.stringify(sendbackdata));
                        } else {
                            sendbackdata.status = "za dużo graczy"
                            res.end(JSON.stringify(sendbackdata));
                        }
                    })
                    break;
                }
                case "/wait": {
                    var allData = "";
                    req.on("data", function (data) {
                        console.log("data: " + data)
                        allData += data;
                    })
                    req.on("end", function (data) {
                        var finish = qs.parse(allData)
                        var d = false;
                        for (var i = 0; i < users.length; i++) {
                            if (users[i].session == finish.session) {
                                d = true;
                                if (users.length == 2) {
                                    res.end("ok")
                                } else {
                                    res.end("wait")
                                }
                            }
                        }
                        if (!d) {
                            res.end("zła sesja")
                        }
                    })

                    break;
                }
                case "/checksession": {
                    var allData = "";
                    req.on("data", function (data) {
                        console.log("data: " + data)
                        allData += data;
                    })
                    req.on("end", function (data) {
                        var finish = qs.parse(allData)
                        var d = false;
                        for (var i = 0; i < users.length; i++) {
                            if (users[i].session == finish.session) {
                                d = true;
                                res.end("ok")
                            }
                        }
                        if (!d) {
                            res.end("zła sesja")
                        }
                    })

                    break;
                }
                case "/getpawns": {
                    var allData = "";
                    req.on("data", function (data) {
                        console.log("data: " + data)
                        allData += data;
                    })
                    req.on("end", function (data) {
                        var finish = qs.parse(allData)
                        var d = false;
                        for (var i = 0; i < users.length; i++) {
                            if (users[i].session == finish.session) {
                                d = true;
                                res.end(JSON.stringify({pawns:pawns,move:ruch}))
                            }
                        }
                        if (!d) {
                            res.end("zła sesja")
                        }
                    })

                    break;
                }
                case "/setpawns": {
                    var allData = "";
                    req.on("data", function (data) {
                        console.log("data: " + data)
                        allData += data;
                    })
                    req.on("end", function (data) {
                        var finish = JSON.parse(allData)
                        var d = false;
                        for (var i = 0; i < users.length; i++) {
                            if (users[i].session == finish.session) {
                                d = true;
                                console.log(finish)
                                pawns=finish.pawns;
                                ruch=finish.move;
                                res.end()
                            }
                        }
                        if (!d) {
                            res.end("zła sesja")
                        }
                    })

                    break;
                }
            }
            break;
        }
    }
})

server.listen(3000, function () {
    console.log("serwer startuje na porcie 3000")
});




var generate_ssid = () => {
    return Math.random().toString();
}