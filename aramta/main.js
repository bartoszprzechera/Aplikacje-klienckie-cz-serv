var http = require("http");
var fs = require('fs')

var server = http.createServer(function (req, res) {
    switch (req.url) {
        case "/": {
            fs.readFile('public/index.html', function (error, data) {
                res.writeHead(200, { 'Content-Type': 'text/html' });
                res.write(data);
                res.end();
            })
            break;
        }
        case "/libs/three.js": {
            fs.readFile('public/libs/three.js', function (error, data) {
                res.writeHead(200, { 'Content-Type': 'text/javascript' });
                res.write(data);
                res.end();
            })
            break;
        }
        case "/js/main.js": {
            fs.readFile('public/js/main.js', function (error, data) {
                res.writeHead(200, { 'Content-Type': 'text/javascript' });
                res.write(data);
                res.end();
            })
            break;
        }
        case "/libs/jquery.js": {
            fs.readFile('public/libs/jquery.js', function (error, data) {
                res.writeHead(200, { 'Content-Type': 'text/javascript' });
                res.write(data);
                res.end();
            })
            break;
        }
        case "/js/aramta.js": {
            fs.readFile('public/js/armata.js', function (error, data) {
                res.writeHead(200, { 'Content-Type': 'text/javascript' });
                res.write(data);
                res.end();
            })
            break;
        }
        case "/js/Kula.js": {
            fs.readFile('public/js/Kula.js', function (error, data) {
                res.writeHead(200, { 'Content-Type': 'text/javascript' });
                res.write(data);
                res.end();
            })
            break;
        }
        case "/js/UI.js": {
            fs.readFile('public/js/UI.js', function (error, data) {
                res.writeHead(200, { 'Content-Type': 'text/javascript' });
                res.write(data);
                res.end();
            })
            break;
        }
        case "/css/main.css": {
            fs.readFile('public/css/main.css', function (error, data) {
                res.writeHead(200, { 'Content-Type': 'text/css' });
                res.write(data);
                res.end();
            })
            break;
        }
    }



})



server.listen(3000, function () {
    console.log("serwer startuje na porcie 3000")
});
var socketio = require("socket.io")
var io = socketio.listen(server)
var clients=[]
io.sockets.on("connection", function (client) {
    
    console.log("klient się podłączył " + client.id)
    // client.id - unikalna nazwa klienta generowana przez socket.io
    client.state={}
    client.state.pos=[0,0,Math.random()*140]
    client.state.rot=0
    client.state.rotluf=0
    client.state.id=client.id
    var t_r=[]
    for(var i=0;i<clients.length;i++){
        t_r.push(clients[i].state)
    }
    client.emit('init',{pos:client.state.pos,others_state:t_r})
    
    for(var i=0;i<clients.length;i++){
        clients[i].emit('newplayer',{state:client.state,id:client.id})
    }
    clients.push(client)
    client.on('onconnect', (e) => {
        console.log(e)
    })
    client.on('rotluf', (e) => {
        client.state.rotluf=e.r
        for(var i=0;i<clients.length;i++){
            clients[i].emit('protluf',{r:client.state.rotluf,id:client.id})
        }
    })
    client.on('rotluf', (e) => {
        client.state.rotluf=e.r
        for(var i=0;i<clients.length;i++){
            clients[i].emit('protluf',{r:client.state.rotluf,id:client.id})
        }
    })
    client.on('rotd', (e) => {
        client.state.rot=e.r
        for(var i=0;i<clients.length;i++){
            clients[i].emit('protd',{r:client.state.rot,id:client.id})
        }
    })
    client.on('fire', (e) => {
        for(var i=0;i<clients.length;i++){
            clients[i].emit('pfire',{id:client.id})
        }
    })
    client.on('reload', (e) => {
        for(var i=0;i<clients.length;i++){
            clients[i].emit('preload',{id:client.id})
        }
    })
    client.on("disconnect", function () {
        for(var i=0;i<clients.length;i++){
            clients[i].emit('removeplayer',{id:client.state.id})
        }
        for(var i=0;i<clients.length;i++){
            console.log(clients[i].id==client.id)
            if(clients[i].id==client.id){
                clients.splice(i,1)
            }
        }
       
        console.log("klient się rozłącza"+clients.length)
    })
})