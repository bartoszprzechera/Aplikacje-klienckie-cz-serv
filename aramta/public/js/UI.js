class UI{
    constructor(armata){
        this.armata=armata;

        this.ui=$('<div>')
        this.ui.attr('class','ui');
        this.rot =$('<input>');
        this.rot.attr('type','range')
        this.rot.attr('min','0')
        this.rot.attr('max','360')
        this.rot.on('input',(e)=>{
            console.log(e.target.value)
            client.emit('rotd',{r:e.target.value})
            this.armata.rotatey(e.target.value);
        })

        this.lufrot=$('<input>');
        this.lufrot.attr('type','range')
        this.lufrot.attr('min','0')
        this.lufrot.attr('max','90')
        this.lufrot.on('input',(e)=>{
            console.log(e.target.value)
            client.emit('rotluf',{r:e.target.value})
            this.armata.rotatelufy(e.target.value);
        })
        this.ui.append(this.rot);
        this.ui.append(this.lufrot);

        this.firebutt=$('<button>')
        this.firebutt.html('fire')
        this.firebutt.on('click',(e)=>{
            client.emit('fire')
            this.armata.fire();
           
        })
        this.reloadbutt=$('<button>')
        this.reloadbutt.html('relaod')
        this.reloadbutt.on('click',(e)=>{
            client.emit('reload')
            this.armata.reload();
        })
        this.ui.append(this.firebutt);
        this.ui.append(this.reloadbutt);
        $('body').append(this.ui)
    }
}