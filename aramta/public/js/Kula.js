class Kula{
    constructor(armata,pos,zak){
        this.pos=pos;
        this.geometry = new THREE.SphereGeometry( 5, 32, 32 );
        this.material = new THREE.MeshBasicMaterial( {color: 0xffff00} );
        this.kula = new THREE.Mesh(  this.geometry, this. material );
        this.kula.position.set(pos.x,pos.y,pos.z)
        if(zak==true){
            armata.firepos.add(this.kula);
        }else{
            scene.add(this.kula);
        }
        
    }
    fire(vector){
        var d=20;
        console.log(vector)
        this.vector= new THREE.Vector3(vector.x,vector.y,vector.z);

        var vec=new THREE.Vector3(0,1,0)
        vec.applyAxisAngle(new THREE.Vector3(1,0,0),this.vector.x)
        vec.applyAxisAngle(new THREE.Vector3(0,1,0),this.vector.y)
        vec.applyAxisAngle(new THREE.Vector3(0,0,1),this.vector.z)
        vec.normalize()
        this.vector=vec
        fired.push(this)
    }
    fired(){
        var d=3;
        console.log(this.vector)
        this.chpos({x:this.kula.position.x+this.vector.x*d,y:this.kula.position.y+this.vector.y*d,z:this.kula.position.z+this.vector.z*d})
    }
    gravity(){
        this.vector.setY(this.vector.y-gravity_w)
    }
    chpos(pos){
        if(pos.y<0){
            scene.remove(this.kula)
            for(var i=0;i<fired.length;i++){
                if(fired[i]==this){
                    fired.splice(i,1)
                }
            }
            
        }
        this.pos=pos;
        this.kula.position.setX(pos.x)
        this.kula.position.setY(pos.y)
        this.kula.position.setZ(pos.z)
        
        
        
    }
    
    get obj(){
        return this.kula
    }
}