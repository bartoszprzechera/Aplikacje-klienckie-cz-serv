import React, { Component } from 'react';
import { Button, FlatList, Text, View,Image,StyleSheet } from 'react-native';
import {Settings} from './Settings'



const styles = StyleSheet.create({
  imgs:{
    flex: 0.2,
    width: 50,
    height: 50,
    resizeMode: 'contain' },
    view:{
      flexDirection: 'row',
    },
    rest:{
      flex:0.4,
    }
})






export class ListItem extends Component {

  constructor(props) {
    super(props);
    this.Delete = this.Delete.bind(this);
    this.Modify = this.Modify.bind(this);
    this.state = { refresh: true }
    this.setState({ refresh: true })
  }
  Modify() {
    this.props.modify(this.props.username, this.props.password)
    console.log("Modify")

  }
  Delete() {
    console.log("Delete")
    this.props.delete(this.props.username, this.props.password)
  }
  render() {
    return (
      <View style={styles.view}> 
        <Image
          source={require('../assets/img/default.png')}
          style={styles.imgs}
        />
        <Text style={styles.rest}>{ this.props.username}</Text>
        <Text style={styles.rest}>{ this.props.password}</Text>
        <Button style={styles.rest} onPress={this.Delete} title="Delete"></Button>
        <Button style={styles.rest} onPress={this.Modify} title="Modify"></Button>
      </View>
    )
  }
}


export default class Users extends Component {
  constructor(props) {
    super(props);
    this.data = [
      { username: 'username1', password: "password1" },
      { username: 'username2', password: "password2" }
    ]
    this.renderItem = this.renderItem.bind(this);
    this._keyExtractor = this._keyExtractor.bind(this);
    this.itemmodify = this.itemmodify.bind(this);
    this.itemDelete = this.itemDelete.bind(this);
    this.reload = this.reload.bind(this);
    this.reload();
  }
  reload() {
    var that = this
    fetch(Settings.url+':'+Settings.port+'/get', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        req: {},
      })
    }).then((response) => response.json())
      .then((responseJson) => {
        console.log(responseJson)
        that.data = responseJson.data
        that.setState({
          refresh: true
        })
      })
      .catch((error) => {
        console.error(error);
        return null
      });
  }
  _keyExtractor = (item, index) => item.id;
  itemmodify(username, passowrd) {
    console.log(username, passowrd)
    this.props.navigation.navigate("EditUser", { username: username, passowrd: passowrd })
  }
  itemDelete(username, password) {
    fetch(Settings.url+':'+Settings.port+'/delete', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        req: {username:username,password:password},
      })
    }).then((response) => response.json())
      .then((responseJson) => {
       this.reload()
      })
      .catch((error) => {
        console.error(error);
        return null
      });
  }
  renderItem = ({ item }) => (<ListItem id={item.id} username={item.username} password={item.password} modify={this.itemmodify} delete={this.itemDelete} />)
  render() {

    return (
      <FlatList extraData={this.state} data={this.data} renderItem={this.renderItem} keyExtractor={this._keyExtractor} />
    );
  }
}
