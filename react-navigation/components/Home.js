import React, { Component } from 'react';
import { Button, TextInput, View, Alert } from 'react-native';
import {Settings} from './Settings'
export default class Home extends Component {
    constructor(props) {
        super(props);
        this.state = {
            login: '',
            password: ''
        }
        this.rejestruj = this.rejestruj.bind(this);
    }
    rejestruj() {
        console.log(Settings.url+':'+Settings.port+'/register')
        fetch(Settings.url+':'+Settings.port+'/register', {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                username: this.state.login,
                password: this.state.password,
            })
        }).then((response) => response.json())
            .then((responseJson) => {
                console.log(responseJson)
                if (responseJson.status == "OK") {
                    this.props.navigation.navigate("Users")
                } else {
                    Alert.alert(
                        'Error',
                        responseJson.status,
                        [
                            { text: 'OK' },
                        ],
                        { cancelable: false }
                    )
                }
            })
            .catch((error) => {
                console.error(error);
                return null
            });
    }
    render() {
        return (
            <View>
                <TextInput placeholder="login" onChangeText={(text) => this.setState({ login: text })} />
                <TextInput secureTextEntry={true} textContentType='password' placeholder="password" onChangeText={(text) => this.setState({ password: text })} />
                <Button
                    title="Rejestruj"
                    onPress={this.rejestruj}
                />
            </View >
        );
    }
}