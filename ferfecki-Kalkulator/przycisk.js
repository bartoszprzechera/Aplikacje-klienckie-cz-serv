import React from 'react';
import { Text, TouchableOpacity, StyleSheet } from 'react-native';

export class Przycisk extends React.Component {
    constructor(props) {
        super(props)
        this.onPress = this.onPress.bind(this);
    }
    onPress() {
        this.props.pressfunction(this.props.presskey)
    }
    render() {
        return (
            <TouchableOpacity style={this.props.styl} onPress={this.onPress}>
                <Text style={styles.buttonText}>{this.props.presskey}</Text>
            </TouchableOpacity>
        )
    }
}
const styles = StyleSheet.create({
    buttonText: {
        fontSize: 45,
    },
})