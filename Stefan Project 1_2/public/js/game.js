
var raycaster, mouseVector, selected
var side, playerr, ruch, zaznaczone = [];
var net = new Net()
w = $(window).width()
h = $(window).height()

var camera = new THREE.PerspectiveCamera(
    90,
    w / h,
    0.1,
    100000,
);

camera.position.x = 700;
camera.position.y = 300;
camera.position.z = 0;

var test = 10;
var gracz = 0;

var klik = 0;
var tab = [0, 0];

var scene = new THREE.Scene();
var karty = [
    [1, 0, 1, 0, 1, 0, 1, 0],
    [0, 1, 0, 1, 0, 1, 0, 1],
    [1, 0, 1, 0, 1, 0, 1, 0],
    [0, 1, 0, 1, 0, 1, 0, 1],
    [1, 0, 1, 0, 1, 0, 1, 0],
    [0, 1, 0, 1, 0, 1, 0, 1],
    [1, 0, 1, 0, 1, 0, 1, 0],
    [0, 1, 0, 1, 0, 1, 0, 1],
];

var tab_plansza = [1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6, 7, 7, 8, 8];
function shuffle(array) {
    var currentIndex = array.length, temporaryValue, randomIndex;
    while (0 !== currentIndex) {
        randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex -= 1;
        temporaryValue = array[currentIndex];
        array[currentIndex] = array[randomIndex];
        array[randomIndex] = temporaryValue;
    }
    return array;
}
var scene = new THREE.Scene();

camera = new THREE.PerspectiveCamera(
    50,
    $(window).width() / $(window).height(),
    0.1,
    10000
);
var renderer = new THREE.WebGLRenderer();
renderer.setClearColor(0xFFFFFF);
renderer.setSize($(window).width(), $(window).height());

$("#root").append(renderer.domElement);

var blackplace = new THREE.MeshPhongMaterial({
    map: new THREE.TextureLoader().load('mats/card.jpg'),
    specular: 0x000000,
    shininess: 0.01,
});
var background = new THREE.MeshPhongMaterial({
    map: new THREE.TextureLoader().load('mats/sel.jpg'),
    specular: 0x000000,
    shininess: 0.01,
});
var tab2_plansza = [], plansza = []
net.newgame()
var light = new THREE.SpotLight(0xffffff, 0.1, 50, 3.14);
light.position.set(0, 500, 0);
scene.add(light);
var loader = new THREE.ObjectLoader();
loader.load('/mats/model.json', function (object) {
    var modelMaterial = new THREE.MeshPhongMaterial(
        {
            map: THREE.ImageUtils.loadTexture("/mats/Wood.jpg"),
            morphTargets: true,
            specular: 0x000000,
            shininess: 0.01,
        });
    object.material = modelMaterial
    object.name = "table";
    object.rotation.set(0, 0, 0)
    object.position.set(0, 0, 0)  // ustaw pozycje modelu
    object.scale.set(2.5, 2, 2); // ustaw skalę modelu
    scene.add(object);

})




var posX = 700;
var z_c=[]
function init() {
    try {
        for (var i = 0; i < tab_plansza.length; i++) {
            for (var j = 0; j < tab_plansza[i].length; j++) {
                scene.remove(tab2_plansza[i][j])
            }
        }
    } catch (e) {

    }

    tab2_plansza = []
    posX = 700;
    
    for (var i = 0; i < 4; i++) {
        var posY = 700;
        tab2_plansza.push([])
        for (var j = 0; j < 4; j++) {
            if (tab_plansza[i][j] != 'hit') {


                var materials = [];

                materials.push(blackplace);
                materials.push(blackplace);
                materials.push(blackplace);
                materials.push(blackplace);
                materials.push(new THREE.MeshPhongMaterial({
                    side: THREE.DoubleSide, map: new THREE.TextureLoader().load('mats/a' + tab_plansza[i][j] + '.jpg'),
                    specular: 0x000000,
                    shininess: 0.01,
                }));
                materials.push(blackplace);
                var pole = new THREE.BoxGeometry(150, 120, 10)
                pole.rotateX(Math.PI / 2);
                var cube = new THREE.Mesh(pole, materials)
                cube.position.set(posX, 0, posY)
                console.log(zaznaczone, { x: j, y: i })
                if (zaznaczone.length == 1) {
                    if (zaznaczone[0].x == j && zaznaczone[0].y == i) {
                        console.log("zaznaczone")
                        cube.position.y = 50;
                        cube.rotateX(Math.PI)
                    }
                } else if (zaznaczone.length == 2) {
                    if ((zaznaczone[0].x == j && zaznaczone[0].y == i) || (zaznaczone[1].x == j && zaznaczone[1].y == i)) {
                        console.log("zaznaczone")
                        cube.position.y = 50;
                        cube.rotateX(Math.PI)
                        z_c.push(cube)
                        setTimeout(() => {
                           for(var i=0;i<z_c.length;i++){
                            z_c[i].position.y = 0;
                            z_c[i].rotateX(-Math.PI)
                           }
                           z_c=[]
                        }, 2000)
                    }
                }

                scene.add(cube)
                cube.karta = { x: j, y: i }

                console.log(cube)
                tab2_plansza[i].push(cube)
            }
            posY -= 200;
        }
        posX -= 200;
    }


}

var pposX = 350;
//===================================================================================================================================mechanika obracania======================================================================================
var ruch = function () {
    raycaster = new THREE.Raycaster()
    mouseVector = new THREE.Vector2()

    $(document).mousedown(function (e) {

        mouseVector.x = (e.clientX / $(window).width()) * 2 - 1
        mouseVector.y = -(e.clientY / $(window).height()) * 2 + 1

        raycaster.setFromCamera(mouseVector, camera)


        var intersects = raycaster.intersectObjects(scene.children)
        var lastSelected = selected



        selected = intersects.length > 0 ? intersects[0].object : undefined

        console.log(selected.position.y)
        if (selected.position.y == 0 && klik <= 1) {
            // selected.position.y = 50;
            // selected.rotateX(Math.PI)
            net.move(selected.karta, playerr)
            // tab[klik] = selected;
            // klik++;

        }
        // else{
        //     klik = 0;
        //     for(i=0;i<tab.length;i++){
        //         tab[i]=0;
        //     }
        // }
        //================================================================================================================================================================================================================

    })
}

// $(document).mousedown(function(e) {
//     mouseVector.x = (e.clientX / $(window).width()) * 2 - 1
//     mouseVector.y = -(e.clientY / $(window).height()) * 2 + 1

//     console.log("mouseVector.x")
//     console.log(mouseVector.x)
//     console.log("mouseVector.y")
//     console.log(mouseVector.y)

//     raycaster.setFromCamera(mouseVector, camera)

// })

//var axes = new THREE.AxesHelper(1000)
//scene.add(axes)

var str = "f";

camera.position.set(1500, 1200, 0)


function render() {
    camera.lookAt(scene.position)
    requestAnimationFrame(render);
    renderer.render(scene, camera)
}
ruch();

render();


client.on('newgame_response', (data) => {
    console.log('newgame_response', (data))
    playerr = 0;
    tab_plansza = data.plansza
    zaznaczone = data.zaznaczone
    init()
})
client.on('join_response', (data) => {
    console.log('join_response', (data))
    playerr = 1;
    tab_plansza = data.plansza
    zaznaczone = data.zaznaczone
    init()
})
client.on('move_response', (data) => {
    console.log('move_response', (data))
    tab_plansza = data.plansza
    zaznaczone = data.zaznaczone
    init()
})
client.on("wyniki", (data) => {
    console.log("wyniki", (data))
    if (data.winner == playerr) {
        alert('win')
    } else if (data.winner == 2) {
        alert('draw')
    } else {
        alert('lost')
    }

})