import { createStackNavigator } from "react-navigation";
import Home from "./components/Home"
import Main from "./components/Main"
import Maps from "./components/Maps"

const App = createStackNavigator({
  Home: { screen: Home },
  Main: { screen: Main },
  Maps: { screen: Maps }
});

export default App;


