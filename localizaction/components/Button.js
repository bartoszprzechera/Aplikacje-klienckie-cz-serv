import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Button } from 'react-native';

class MyButton extends Component{
    constructor(props){
        super(props)
    }
    render(){
        return(
            <Button title={this.props.name} onPress={this.props.onclick}/>
        )
       
    }
}
MyButton.propTypes = {
    name: PropTypes.string.isRequired,
    onclick:PropTypes.func.isRequired,
};
export default MyButton