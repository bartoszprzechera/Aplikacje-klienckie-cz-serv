import React, { Component } from 'react';
import { Button, FlatList, Text, View, Image, ActivityIndicator, TouchableOpacity } from 'react-native';
import { MapView } from 'expo';

export default class Maps extends Component {
    constructor(props) {
        super(props)
    }
    render() {
        console.log("--------------------------------------")
        console.log(this.props.navigation.state.params.zazn)
        console.log("--------------------------------------")
        var arr = []
        for (var i = 0; i < this.props.navigation.state.params.zazn.length; i++) {
            arr.push(<MapView.Marker
                coordinate={{
                    latitude: this.props.navigation.state.params.zazn[i].lat,
                    longitude: this.props.navigation.state.params.zazn[i].long,
                }}
                title={this.props.navigation.state.params.zazn.timestamp}
                description={"desc"}
            />)
        }

        return (
            <MapView
                style={{ flex: 1 }}
                initialRegion={{
                    latitude: 50.111,
                    longitude: 20.111,
                    latitudeDelta: 0.001,
                    longitudeDelta: 0.001,
                }}
            >
                {arr}
            </MapView>
        )
    }
}