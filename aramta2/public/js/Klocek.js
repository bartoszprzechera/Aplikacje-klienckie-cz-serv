class Klocek{
    constructor(pos){
        this.pos=pos
        
        this.kloc = new THREE.BoxGeometry(10,10,10);
        this.material = new THREE.MeshBasicMaterial({
            color: 0xff0000,
            wireframe: true
        });
        this.obj=new THREE.Mesh(this.kloc, this.material);
        this.obj.position.set(pos.x,pos.y,pos.z)
        scene.add(this.obj)
    }
}