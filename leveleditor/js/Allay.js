class Allay {
    constructor(player, position) {
        this.maxodl = 20;
        this.container = new THREE.Object3D();
        this.mesh = player.getPlayerMesh().getmesh().clone()
        this.mesh.name = 'allay';
        this.container.add(this.mesh)
        this.axes = new THREE.AxesHelper(50)
        this.container.add(this.axes)
        this.container.position.set(position.x, position.y, position.z)
        this.mixer = new THREE.AnimationMixer(this.mesh);
        var geometry = new THREE.RingGeometry(10, 5, 64);
        var material = new THREE.MeshBasicMaterial({ color: 0xffff00, side: THREE.DoubleSide });
        this.znak = new THREE.Mesh(geometry, material);
        this.znak.rotation.x = Math.PI * 2 / 360 * 90
        this.znak.position.y += 0.1
        this.znak.visible = false;
        this.container.add(this.znak)

        scene.add(this.container)
        this.animate('stand')
    }
    setfollowtarget(target) {
        if (this.target == undefined) {
            this.znak.visible = true;
            this.target = target
            this.directionVect = this.target.position.clone().sub(this.container.position).normalize()
        } else {
            this.target = undefined;
            this.znak.visible = false;
            this.animate('stand')
        }

    }
    animate(anim) {
        try {
            //console.log(this.prev, anim)
            this.mixer.update(0)
            this.mixer.existingAction(this.prev).stop();
        } catch (e) {
            //console.log(e)
        }
        this.mixer.clipAction(anim).play();
        this.prev = anim;
    }
    updateModel(delta) {

        if (this.mixer) this.mixer.update(delta)
    }
    follow() {
        if (this.target != undefined) {
            console.log(this.prev)
            var angle = Math.atan2(
                this.container.position.clone().x - this.target.position.x,
                this.container.position.clone().z - this.target.position.z
            )
            this.mesh.rotation.y = angle - Math.PI * 2 / 360 * 90;
            this.directionVect = this.target.position.clone().sub(this.container.position).normalize()
            if (Math.floor(this.target.position.clone().distanceTo(this.container.position)) > this.maxodl) {
                this.container.translateOnAxis(this.directionVect, 0.6)

                if (this.prev != 'run') {
                    this.animate('run')
                }
            } else {
                this.directionVect.x = 0;
                this.directionVect.y = 0;
                this.directionVect.z = 0;
                if (this.prev != 'stand') {
                    this.animate('stand')
                }
            }
        }

    }
    getmesh() {
        return this.mesh
    }
}