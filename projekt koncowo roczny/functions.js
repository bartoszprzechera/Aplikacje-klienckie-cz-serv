var assert = require('assert');
var ObjectID = require('mongodb').ObjectID;
var connect = require('./connect');
var config = require('./config.json');
const CROSS = 1, CIRCLE = 2, EMPTY = 0;
const colors = ['blue', 'red', 'green', 'gray', 'purple', 'orange', 'black']

var changePassword = function (session, login, oldpassword, newpassword) {
    try {
        getSessionOwnerIfValid(session, function (dane) {
            var o_id = new ObjectID(dane);
            connect.mongo_update('Konta', {
                "login": login,
                "_id": o_id,
                "password": oldpassword
            }, {
                    $set: {
                        "password": newpassword
                    }
                }, function (data) {
                    if (data) {
                        sendEmailToAccount(dane, config.email, "Password was changed", "Pasword to your acount was change from:" + oldpassword + " ,to: " + newpassword + " .", "");
                        extendSessionTime(session);
                    } else {

                    }
                });
        });
    }
    catch (e) {
        console.log(e);
    }
};
var removeAccount = function (session, login, password) {
    try {
        getSessionOwnerIfValid(session, function (dane) {
            var o_id = new ObjectID(dane);
            connect.mongo_delete('Konta', {
                "_id": o_id,
                "login": login,
                "password": password
            }, function (data) {

            })
        });
    }
    catch (e) {
        console.log(e);
    }
};
var createSession = function (owner, callback) {
    try {
        var id = new ObjectID();
        var date = new ObjectID().getTimestamp();
        connect.mongo_insert('Sessions', {
            "_id": id,
            "owner": owner,
            "valid_date": date
        }, function (data) {
            if (data)
                callback(id);
        })
    }
    catch (e) {
        console.log(e);
    }
};
var logowanie = function (login, password, callback) {
    try {
        connect.mongo_find('Konta', {
            "login": login,
            "password": password
        }, function (data) {
            if (data != null && data != false) {
                if (data[0].status != "new") {
                    createSession(data[0]._id, function (dane) {
                        callback(dane);
                    });
                } else {
                    callback('Verifacation required');
                }
            } else {
                callback("failed");
            }
        })
    }
    catch (e) {
        console.log(e);
        callback("error");
    }
};
var verify = function (login, mail, callback) {
    try {
        connect.mongo_update('Konta', {
            "login": login,
            "mail": mail,
            "status": "new"
        }, {
                $set: {
                    "status": "verified"
                }
            }, function (data) {
                if (data) {
                    connect.mongo_find('Konta', {
                        "login": login
                    }, function (data2) {
                        if (data2 != null && data2 != false) {
                            sendEmailToAccount(data2[0]._id, config.email, "Account Activaction", "Your account was succesfull activated.", "");
                            callback(true);
                        }
                    })
                } else {
                    callback(false);
                }

            })
    }
    catch (e) {
        console.log(e);
        callback(false);
    }
};
var registerAccount = function (login, password, mail, callback) {
    try {
        connect.mongo_find('Konta', {
            $or: [{ "login": login }, { "email": mail }]
        }, function (data) {
            if (data == null || data == false) {
                connect.mongo_insert('Konta', {
                    "login": login,
                    "password": password,
                    "mail": mail,
                    "status": "new"
                }, function (data2) {
                    if (data2) {
                        callback("success")
                        sendEmail(mail, config.email, "Verify your TICTACTOE account", "", "<a href='" + config.server_address + "/verify?login=" + login + "&mail=" + mail + "'>Verify your account</a>");
                    } else {
                        callback("failed")
                    }
                })
            } else {
                callback("failed")
            }
        })
    }
    catch (e) {
        callback("failed")
        console.log(e);
    }
};
var getSessionOwnerIfValid = function (session_id, callback) {
    try {
        var id = new ObjectID(session_id);
        connect.mongo_find('Sessions', {
            "_id": id
        }, function (data) {
            if (data != null && data != false) {
                try {
                    var date_session = new Date(ObjectID(id).getTimestamp());
                    var data_now = new Date(ObjectID().getTimestamp());
                    var diff = data_now - date_session;
                    //console.log(Math.floor(diff / 60e3))
                    if (diff > 60e3) {
                        if (Math.floor(diff / 60e3) <= 10080) {
                            callback(data[0].owner);
                        }
                    }
                    else {
                        callback(data[0].owner);
                    }
                }
                catch (e) {
                    console.log(e);
                    callback(undefined);
                }
            } else {
                callback(undefined);
            }
        })
    }
    catch (e) {
        console.log(e);
    }
};
var sendEmailToAccount = function (user, odkogo, temat, wiadomosc, html) {
    try {
        'use strict';
        var dokogo;
        const nodemailer = require('nodemailer');
        let transporter = nodemailer.createTransport({
            service: 'gmail',
            auth: {
                user: config.email_login,
                pass: config.email_password
            }
        });
        var o_id = new ObjectID(user);
        connect.mongo_find('Konta', {
            "_id": o_id
        }, function (data) {
            if (data != null && data != false) {
                try {
                    dokogo = data[0].mail.toString();
                    let mailOptions = {
                        from: odkogo, // sender address
                        to: dokogo, // list of receivers
                        subject: temat, // Subject line
                        text: wiadomosc, // plain text body
                        html: html // html body
                    };
                    transporter.sendMail(mailOptions, (error, info) => {
                        if (error) {
                            return console.log(error);
                        }
                        console.log('Message %s sent: %s', info.messageId, info.response);
                    });
                }
                catch (e) {
                    console.log(e);
                }
            }
        })
    }
    catch (e) {
        console.log(e);
    }
};
var sendEmail = function (dokogo, odkogo, temat, wiadomosc, html) {
    try {
        'use strict';
        const nodemailer = require('nodemailer');
        let transporter = nodemailer.createTransport({
            service: 'gmail',
            auth: {
                user: config.email_login,
                pass: config.email_password
            }
        });
        let mailOptions = {
            from: odkogo, // sender address
            to: dokogo, // list of receivers
            subject: temat, // Subject line
            text: wiadomosc, // plain text body
            html: html // html body
        };
        transporter.sendMail(mailOptions, (error, info) => {
            if (error) {
                return console.log(error);
            }
            console.log('Message %s sent: %s', info.messageId, info.response);
        });
    }
    catch (e) {
        console.log(e);
    }
};
var extendSessionTime = function (session_id) {
    try {
        var data_now = new Date(ObjectID().getTimestamp());
        var id = new ObjectID(session_id);
        connect.mongo_update('Sessions', {
            "_id": id,
        }, {
                $set: {
                    "valid_date": data_now
                }
            }, function () { })
    }
    catch (e) {
        console.log(e);
    }
};












function getTable2D(x, y, callback) {
    var tab = [];
    for (var i = 0; i < x; i++) {
        tab[i] = new Array (y);
    }
    for (var i = 0; i < x; i++) {
        for (var c = 0; c < y; c++) {
            tab[i][c] = 0
        }
    }
    callback(tab);
}
function getRandomArbitrary(min, max) {
    return Math.round(Math.random() * (max - min) + min);
}
function losuj_nastepny() {
    var wynik = [];
    for (var i = 0; i < 3; i++) {
        var rand_color = getRandomArbitrary(0, colors.length - 1)
        //var rand_color = 0
        //console.log(rand_color)
        wynik.push(rand_color);
        // var div_t = document.createElement('div');
        // div_t.setAttribute('style', 'background-color:' + colors[rand_color])
        // div_t.setAttribute('class', 'kulka_p')
        // next.appendChild(div_t);
    }
    return wynik;
}
var createGame = function (session_id, room_name, width, height, howmanytowin, callback) {
    try {
        extendSessionTime(session_id)
        get_board_and_whos_move_and_players_howmanytowin(room_name, function (data) {
            if (data == "error") {
                getSessionOwnerIfValid(session_id, function (data) {
                    getTable2D(width, height, function (data2) {
                        for (var i = 0; i < 3; i++) {
                            var randh = getRandomArbitrary(0, height - 1);
                            var randw = getRandomArbitrary(0, width - 1);
                            var rand_color = getRandomArbitrary(0, colors.length - 1)
                            if (data2[randh][randw] == 0) {
                                data2[randh][randw] = rand_color;
                            } else {
                                i--
                            }
                        }
                        var nast = losuj_nastepny()
                        connect.mongo_insert('Games', {
                            "player1": data,
                            "player2": null,
                            "room_name": room_name,
                            "board": data2,
                            "whos_move": CROSS,
                            "next_move": nast,
                            "p1_p": 0,
                            "p2_p": 0,
                            "howmanytowin": howmanytowin
                        }, function (data3) {
                            if (data3) {
                                callback("success")
                            } else {
                                callback("failed")
                            }
                        })
                    });
                })
            } else {
                callback("failed")
            }
        })
    }
    catch (e) {
        console.log(e);
        callback("failed")
    }
}
var connectToTheGame = function (session_id, room_name, callback) {
    try {
        extendSessionTime(session_id)
        getSessionOwnerIfValid(session_id, function (data) {
            try {
                connect.mongo_update('Games', {
                    "room_name": room_name,
                    "player2": null
                }, {
                        $set: {
                            "player2": data
                        }
                    }, function (data2) {
                        if (data2) {
                            get_board_and_whos_move_and_players_howmanytowin(room_name, function (data) {
                                if (data != null && data != undefined && data != "error") {
                                    callback("success")
                                } else {
                                    callback("error")
                                }
                            })
                        } else {
                            callback("error")
                        }

                    })
            } catch (e) {
                console.log(e)
                callback("failed")
            }
        });

    }
    catch (e) {
        console.log(e);
        callback("failed")
    }
}
var get_board_and_whos_move_and_players_howmanytowin = function (room_name, callback) {
    try {
        connect.mongo_find('Games', {
            "room_name": room_name
        }, function (data) {
            if (data != null && data != false) {
                console.log(data)
                callback(data[0].board, data[0].whos_move, data[0].player1, data[0].player2, data[0].howmanytowin, data[0].next_move, data[0].p1_p, data[0].p2_p)
            }
            else {
                callback("error")
            }
        })
    }
    catch (e) {
        console.log(e);
        callback("error")
    }
}
var move = function (session_id, room_name, from, to, callback) {
    try {
        extendSessionTime(session_id)
        getSessionOwnerIfValid(session_id, function (data) {
            get_board_and_whos_move_and_players_howmanytowin(room_name, function (board, whos_move, p1, p2, howmanytowin, next_move, points1, points2) {
                if (whos_move == CROSS && ("" + p1).trim() != ("" + data).trim() && ("" + p2).trim() == ("" + data).trim()) {
                    callback("error")
                } else if (whos_move == CIRCLE && ("" + p1).trim() == ("" + data).trim() && ("" + p2).trim() != ("" + data).trim()) {
                    callback("error")
                } else if ((whos_move == CIRCLE && ("" + p1).trim() != ("" + data).trim() && ("" + p2).trim() == ("" + data).trim()) || (whos_move == CROSS && ("" + p1).trim() == ("" + data).trim() && ("" + p2).trim() != ("" + data).trim())) {
                    var id = new ObjectID(data);
                    if (board[to.y][to.x] == 0) {
                        board[to.y][to.x] = board[from.y][from.x]
                        board[from.y][from.x] = 0

                        var p=0
                        for (var i = 0; i < 3; i++) {
                            
                            var randh = getRandomArbitrary(0, board.length - 1);
                            var randw = getRandomArbitrary(0, board[0].length - 1);
                            if (board[randh][randw] == 0) {
                                board[randh][randw] = next_move[i]
                            } else if(p<30){
                                i--
                                p++
                            }
                        }
                        checkBoardForWin(board, howmanytowin, function (reee) {
                            var temp_int = 0
                            for (var i = 0; i < board.length; i++) {
                                for (var j = 0; j < board[i].length; j++) {
                                   
                                    if (board[i][j] == 0) {
                                        temp_int++
                                    }
                                    console.log(board[i][j] == 0,temp_int)
                                }
                            }

                            var nast = losuj_nastepny()
                            console.log(points1, points2, reee)
                            if (whos_move == CROSS) {
                                whos_move = CIRCLE
                                connect.mongo_update('Games', {
                                    "room_name": room_name,
                                    "player1": id
                                }, {
                                        $set: {
                                            "board": board,
                                            "whos_move": whos_move,
                                            "next_move": nast,
                                            "p1_p": points1 + reee
                                        }
                                    },
                                    function (data) {
                                        if (data) {
                                            console.log(temp_int!=0,temp_int)
                                            if (temp_int >= 3) {
                                                callback(board, nast, points1 + reee, points2, 345345)
                                            } else {
                                                if (points1 + reee > points2) {
                                                    callback(board, nast, points1 + reee, points2, 1)
                                                } else if (points1 + reee == points2) {
                                                    callback(board, nast, points1 + reee, points2, 0)
                                                } else if (points1 + reee < points2) {
                                                    callback(board, nast, points1 + reee, points2, 2)
                                                }
                                            }

                                        }
                                    })
                            } else {
                                whos_move = CROSS
                                connect.mongo_update('Games', {
                                    "room_name": room_name,
                                    "player2": id

                                }, {
                                        $set: {
                                            "board": board,
                                            "whos_move": whos_move,
                                            "next_move": nast,
                                            "p2_p": points2 + reee
                                        }
                                    },
                                    function (data) {
                                        if (data) {
                                            console.log(temp_int!=0,temp_int)
                                            if (temp_int >= 3) {
                                                callback(board, nast, points1, points2 + reee, 345345)
                                            } else {
                                                if (points1 > points2 + reee) {
                                                    callback(board, nast, points1, points2 + reee, 1)
                                                } else if (points1 == points2 + reee) {
                                                    callback(board, nast, points1, points2 + reee, 0)
                                                } else if (points1 < points2 + reee) {
                                                    callback(board, nast, points1, points2 + reee, 2)
                                                }
                                            }
                                        }
                                    })
                            }

                        })

                    } else {
                        console.log("error");
                        callback("error")
                    }
                }
            })
        });
    }
    catch (e) {
        console.log(e);
    }
}
var deleteGame = function (room_name) {
    try {
        connect.mongo_delete('Games', {
            "room_name": room_name
        }, function () { })
    }
    catch (e) {
        console.log(e);
    }
}
function checkRightDiagonal(howManyToWin, board) {
    var wynik = [];
    for (var y = 0; y < board[0].length - howManyToWin + 1; y++) {
        for (var x = 0; x < board.length - howManyToWin + 1; x++) {
            var correctCount = 0;
            var sign = board[x][y];
            if (sign != null) {
                for (var i = 0; i < howManyToWin; i++) {
                    if (board[x + i][y + i] == sign)
                        correctCount++;
                }
                if (correctCount >= howManyToWin)
                    wynik.push({ s: { x: x, y: y }, d: correctCount });
            }
        }
    }
    return wynik;
}
function checkHorizontal(howManyToWin, board) {
    var wynik = [];
    for (var y = 0; y < board[0].length; y++) {
        for (var x = 0; x < board.length - howManyToWin + 1; x++) {
            var correctCount = 0;
            var sign = board[x][y];
            if (sign != null) {
                for (var i = 0; i < howManyToWin; i++)
                    if (board[x + i][y] == sign)
                        correctCount++;
                if (correctCount >= howManyToWin)
                    wynik.push({ s: { x: x, y: y }, d: correctCount });
            }
        }
    }
    return wynik;
}
function checkVertical(howManyToWin, board) {
    var wynik = [];
    for (var y = 0; y < board[0].length - howManyToWin + 1; y++) {
        for (var x = 0; x < board.length; x++) {
            var correctCount = 0;
            var sign = board[x][y];
            if (sign != null) {
                for (var i = 0; i < howManyToWin; i++)
                    if (board[x][y + i] == sign)
                        correctCount++;
                if (correctCount >= howManyToWin)
                    wynik.push({ s: { x: x, y: y }, d: correctCount });
            }
        }
    }
    return wynik;
}

function checkLeftDiagonal(howManyToWin, board) {
    var wynik = [];
    for (var y = 0; y < board[0].length - howManyToWin + 1; y++) {
        for (var x = howManyToWin - 1; x < board.length; x++) {
            var correctCount = 0;
            var sign = board[x][y];
            if (sign != null) {
                for (var i = 0; i < howManyToWin; i++)
                    if (board[x - i][y + i] == sign)
                        correctCount++;
                if (correctCount >= howManyToWin)
                    wynik.push({ s: { x: x, y: y }, d: correctCount });
            }
        }
    }
    return wynik;
}
var checkBoardForWin = function (board, howmanytowin, callback) {
    var wynik = 0
    var s1 = checkRightDiagonal(howmanytowin, board)
    var s2 = checkHorizontal(howmanytowin, board)
    var s3 = checkVertical(howmanytowin, board)
    var s4 = checkLeftDiagonal(howmanytowin, board)
    console.log(s1, s2, s3, s4)
    for (var i = 0; i < s1.length; i++) {
        for (var j = 0; j < s1[i].d; j++) {
            if (board[s1[i].s.x + j][s1[i].s.y + j] != 0) {
                wynik++;
            }
            board[s1[i].s.x + j][s1[i].s.y + j] = 0
        }
    }

    for (var i = 0; i < s2.length; i++) {
        for (var j = 0; j < s2[i].d; j++) {
            if (board[s2[i].s.x + j][s2[i].s.y] != 0) {
                wynik++;
            }
            board[s2[i].s.x + j][s2[i].s.y] = 0
        }
    }

    for (var i = 0; i < s3.length; i++) {
        for (var j = 0; j < s3[i].d; j++) {
            if (board[s3[i].s.x][s3[i].s.y + j] != 0) {
                wynik++;
            }
            board[s3[i].s.x][s3[i].s.y + j] = 0
        }
    }

    for (var i = 0; i < s4.length; i++) {
        for (var j = 0; j < s4[i].d; j++) {
            if (board[s4[i].s.x - j][s4[i].s.y + j] != 0) {
                wynik++;
            }
            board[s4[i].s.x - j][s4[i].s.y + j] = 0
        }
    }
    console.log('wynik: ' + wynik)
    callback(wynik)
}
var getSessionValid = function (session_id, callback) {
    getSessionOwnerIfValid(session_id, function (data) {
        if (data != undefined) {
            callback(true);
        }
        else {
            callback(false);
        }
    });
};
var getAllRoomList = function (callback) {
    try {
        connect.mongo_find('Games', {}, function (data) {
            if (data != null && data != false) {
                callback(data)
            } else {
                callback([])
            }
        })
    }
    catch (e) {
        console.log(e);
        callback("error")
    }
}
module.exports.get_board_and_whos_move_and_players_howmanytowin = get_board_and_whos_move_and_players_howmanytowin;
module.exports.getAllRoomList = getAllRoomList;
module.exports.checkBoardForWin = checkBoardForWin;
module.exports.createGame = createGame;
module.exports.move = move;
module.exports.connectToTheGame = connectToTheGame;
module.exports.deleteGame = deleteGame;
module.exports.getSessionValid = getSessionValid;
module.exports.sendEmailToAccount = sendEmailToAccount;
module.exports.sendEmail = sendEmail;
module.exports.changePassword = changePassword;
module.exports.removeAccount = removeAccount;
module.exports.logowanie = logowanie;
module.exports.registerAccount = registerAccount;
module.exports.verify = verify;