var express = require('express');
var router = express.Router();
var config = require("../config.json")
/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('register',  {'address':config.server_address+":"+config.server_port});
});

module.exports = router;
